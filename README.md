# README #

The aim of this project is to design and implement a data structure which supports the following functions

public interface CorrelationQueue{
	Element pop();
	Element pop(String family);
	boolean push(long id, String family);
}
where Element is a pojo with fields `id` and `family`

### Description of the methods

`Element pop()` -  Removes and returns the oldest element of the queue 

`Element pop(String family)` -  Removes and returns the oldest element of the queue which belongs to the family specified.

`boolean push(long id, String family)` - Adds an element to the begining of the queue using the values supplied

`CorrelationQueueImpl(int maxSize)` - The implementation of the queue needs to take a constructor parameter that has one int parameter that is the Maximum Size of the queue.


### How do I get set up? ###
JDK

### Contribution guidelines ###

JUnit 4 test coverage for all methods
