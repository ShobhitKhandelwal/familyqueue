package com.entity;

/**
 * POJO to define elements of the queue with two attributes as id and family
 * @author Shobhit
 *
 */
public class Element {	
	private long id;
	private String family;
			
	public Element(long id, String family) {
		super();
		this.id = id;
		this.family = family;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFamily() {
		return family;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	@Override
	public String toString() {
		return "Element [id=" + id + ", family=" + family + "]";
	}
}
