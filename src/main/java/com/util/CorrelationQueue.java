/**
 * 
 */
package com.util;

import com.entity.Element;

/**
 * A queue data structure that supports element insertion and deletion.
 * This interface defines methods to add and remove elements based on
 * certain conditions .
 * 
 * @author Shobhit
 *
 */
public interface CorrelationQueue {
	
	/*Removes and returns the oldest element of the queue */
	Element pop();
	
	/*Removes and returns the oldest element of the queue which belongs to the family specified*/
	Element pop(String family);
	
	/*Adds an element to the begining of the queue using the values supplied
	 * 
	 * @param id - a long value to identify the family
	 * @param family - name of the family
	 * @return boolean value
	 * 
	*/
	boolean push(long id, String family);
	
	/*return front element of the queue*/
	Element head();
}
