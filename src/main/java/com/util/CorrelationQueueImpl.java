/**
 * 
 */
package com.util;

import java.util.NoSuchElementException;
import com.entity.Element;

/**
 * Array implementation of CorrelationQueue interface. Capacity restriction defined by
 * maxSize parameter in the constructor. Methods are not thread-safe, they don't support 
 * concurrent access by multiple threads when there is no external synchronization
 * 
 * @author Shobhit
 *
 */
public class CorrelationQueueImpl implements CorrelationQueue {
	
	//The array in which elements of the queue are stored
	private Element data[];
	//The counter variable to check on queue addition or deletion
	private int size;
	
	/*Constructs an empty array with maxSize to
	 * hold the specified number of elements
	 * @param maxSize - maximum capacity of the queue
	*/
	public CorrelationQueueImpl(int maxSize){
		if(maxSize < 1) {
			throw new IllegalArgumentException("Queue size must be 1 or greater");
		}		
		data = new Element[maxSize];
	}
	
	@Override
	public Element pop() {
		Element e = null;
		if(size == 0) {
			throw new NoSuchElementException("Queue does not contain any item to remove");
		}
		if(size == data.length) {
			e=data[data.length-1];
			data[data.length-1]=null;
		}else {
			for(int i=0;i<data.length;i++) {
				if(data[i]==null) {
					e = data[i-1];
					data[i-1]=null;
					break;
				}
			}
		}
		size--;	
		System.out.println("Removed " + e.toString());
		return e;		
	}

	@Override
	public Element pop(String family) {
		boolean flag = false;
		Element e = null;
		Element elements[] = null;
		int cap = data.length-1;
		if(size == 0) {
			throw new NoSuchElementException("Queue does not contain any item to remove");
		}
		if(family == null) {
			throw new NoSuchElementException("null not allowed in family");
		}
		
		/*creating a new array if data matches and copy the elements in that array. 
		 * New array have size of data array�s size � 1 so as to shift all the elements, 
		 * that are after the element that has to be removed,
		*/
		for(int i=cap; i>=0; i--) {
			if(data[i]!=null && family.equals(data[i].getFamily())) {
				elements = new Element[cap];
				for(int index=i-1; index>=0; index--) {
					elements[index] = data[index];
				}
				for(int j=i; j<cap; j++) {
				elements[j]=data[j+1];
				}
				e = data[cap];
				data[cap]=null;
				size--;
				flag=true;
				break;
			}
		}
					
		if (!flag) {
			throw new NoSuchElementException("Family "+family +" does not exist!");			
		}
		System.arraycopy(elements,0,data,0,elements.length);
		return e;	
	}

	@Override
	public boolean push(long id, String family) {
		if(size == data.length) {
			throw new IllegalArgumentException("Max capacity of queue reached, no more element can be added");
		}
	    	    	    
	    if(size==0) {
	    	data[0] = new Element(id, family);
	    }else {
			/*
			 * Throw an exception if id within same family already exist
			 */
	    	for(int i=0; i<data.length; i++) {
    			if(data[i]!=null && id==data[i].getId() && family.equals(data[i].getFamily())) {
	    			throw new IllegalArgumentException("Id "+ id + " of family " + family + " already exist. Ids within a family should be unique");
	    		}
    		}
	    	/*
			 * Shift array by one and create space for new element to be added at the begining of the queue
			 */
	    	for(int i=data.length-1;i>0;i--){
	    		data[i]=data[i-1];
	    	}
	    	data[0] = new Element(id, family);
	    }
	    size++;
	    return true;
	}

	//To get the status of the queue
	public void getStatus() {
		if (size == 0) {
            throw new NoSuchElementException("Queue does not contain any items.");
        }
		System.out.println("Queue Status: ");
		for(Element e : data)
			System.out.println(e);
	}
	
	//Returns head element of the queue
	public Element head() {
		System.out.println("Head of the queue at " +data[0]);
		return data[0];
	}
}
