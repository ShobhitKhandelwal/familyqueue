/**
 * 
 */
package com.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.NoSuchElementException;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Shobhit
 *
 */
public class CorrelationQueueTest {

	CorrelationQueue correlationQueueTest;
	
	@Before
	public void init() throws Exception{
		correlationQueueTest = new CorrelationQueueImpl(5);
	}

	@Test
	public void testPop() {
		correlationQueueTest.push(62,"Ambani");
		correlationQueueTest.push(95,"Tata");
		correlationQueueTest.push(88,"Birla");
		assertEquals(62,correlationQueueTest.pop().getId());	
	}

	@Test
	public void testPopString() {
		correlationQueueTest.push(40,"Unix");
		correlationQueueTest.push(40,"Windows");
		assertNull(correlationQueueTest.pop("Windows"));
	}

	@Test
	public void testPush() {
		correlationQueueTest.push(5,"Gates");
		assertEquals(5, correlationQueueTest.head().getId());
		correlationQueueTest.push(11,"Jobs");
		assertEquals(11, correlationQueueTest.head().getId());
		correlationQueueTest.push(07,"Buffet");
		assertEquals(07, correlationQueueTest.head().getId());
		correlationQueueTest.push(21,"Bezos");
		assertEquals(21, correlationQueueTest.head().getId());
		correlationQueueTest.push(28,"Page");
		assertEquals(28, correlationQueueTest.head().getId());
	}
	
	@Test(expected = NoSuchElementException.class)
	public void testPopError() {
		correlationQueueTest.pop();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testPushError() {
		correlationQueueTest.push(1, "Gavaskar");
		correlationQueueTest.push(2, "Dev");
		correlationQueueTest.push(3, "Tendulkar");
		correlationQueueTest.push(4, "Ganguli");
		correlationQueueTest.push(5, "Dravid");
		correlationQueueTest.push(6, "Kohli");
	}
}
